**Cyconia - Exercice de recrutement front-end**

Bonjour et bienvenue sur l’exercice de recrutement front-end de Cyconia.  

Durant ce test, vous allez être confronté à un contexte dans lequel nous vous demanderons de mettre en oeuvre vos compétences et votre savoir afin de proposer une solution.  

L’objectif n’étant pas une réponse « parfaite », vous serez attendu sur vos méthodes et logiques de construction mais aussi sur votre capacité à vous adapter au contexte proposé par ce test.

---

## Pré-requis

Aucun pré-requis pour cet exercice. Vous êtes libre d’effectuer ce test dans les conditions qu’il vous plaira tant que celui-ci respecte les consignes et le cadre défini par l’énoncé plus bas.

---

## Toujours bon à savoir ;)

**Chez Cyconia, la Stack qu’on aime en front-end c’est :**

* React JS
* Redux / Saga
* Styled-component
* Material-ui
* Micro front-end (pour l’architecture)
* Atomic Design (pour le design pattern)

**Côté process de travail, on est sur :**

* Git flow
* CI/CD

---

## Exercice front-end (~3h)

**Description :**  
Dans cet exercice, vous devrez mettre en place un mini système de réservation sur la base d’un ensemble de données. 
Vous devrez créer une page permettant de rechercher et réserver une des salles de réunion selon la disponibilité, la capacité et l'équipement.  

Aucune création ou gestion de compte utilisateur ne vous est demandé mais chaque réservation doit être enregistrée localement.  

Lors d'une recherche, si un utilisateur recherche une même date / heure, la salle de réunion précédemment réservée n'apparaîtra pas dans la liste des salles disponibles.  

À noter que lors d’une nouvelle recherche, toutes les réservations déjà effectuées, pour un jour et une date spécifique, ne devront plus apparaître dans la liste de recherche (car considérées comme indisponible). Vous avez la liberté de les cacher ou de simplement les désactivés selon votre préférence.

**Périmètre :**  
Une base projet `<create-react-app>` muni d’un simple `App.js` et d’un `Rooms.json` (data de l’exercice) a été créé puis mis à votre disposition sur ce repository.  
À noter que cette base projet n’embarque aucun des outils de notre stack et que leur utilisation dans le cadre de ce test n’est pas obligatoire.

**Objectif :**  
Nous jugerons votre capacité à vous adaptez à ce test et à imaginez sa construction.
Sur cette base, vous avez 3h pour répondre à l’exercice en utilisant les méthodes et outils de votre choix.

**Livraison :**  
Une fois terminé, merci de nous transféré l’exercice à l’adresse pascal.liu@cyconia.io et par l’un de ces deux biais :  

* un lien vers l’un de vos repository personnel sur lequel vous aurez disposez le rendu de votre exercice  
* un zip de votre exercice dans lequel vous aurez pris soins de supprimer les `node-modules`  

Si vous avez une question, vous pouvez également écrire à pascal.liu@cyconia.io !

## En espérant que tout est clair, amusez-vous bien !
